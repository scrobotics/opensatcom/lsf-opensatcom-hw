# OpenSatCom HW

This hardware is a PCB designed to attach to a Nucleo64 board and provide analog signal conditioning to its DAC output and a uSD card slot.

<p align="center">
  <img src="./img/analog.jpg" width="800"/>
</p>

Analog section is basically an unbalanced (DAQ output) to balanced (most modulators input) converter with offset correction.

Both voltage dividers R2-R3 and R5-R6 can be used to adjust Common mode Voltage to modulator needs. In this case, it is set to 1.4V for interfacing with XXXX.

R1-C4 and R4-C5 can be used as a simple RC Low Pass Filter if signal smoothing is needed.

<p align="center">
  <img src="./img/uSD_slot.jpg" width="800"/>
</p>

uSD card slot is connected to Nucleo64 board using SPI bus.

<p align="center">
  <img src="./img/nucleo_headers.jpg" width="800"/>
</p>

There are also connectors that can be used to debug signals or to connect with/from external devices.

<p align="center">
  <img src="./img/debug_conn.jpg" width="800"/>
</p>


LICENSE:

OpenSatCom Hardware is licensed under the open-source license CERN-OHL-P v2 (or later). See the following for detail: (https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_p_v2.txt)

